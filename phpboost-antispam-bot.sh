#!/bin/bash

current_run=$(date +%s)

echo 'Info: retrieve configuration.'
for cfg_file in ~/.config/phpboost_antispam_bot /etc/phpboost_antispam_bot ; do
    if [[ -e "${cfg_file}" ]] ; then
        . "${cfg_file}"
        break
    fi
done
# Check variables
for var in LOGIN bot_user PASSWORD webserver data_base filter_user_max_msg reportTopic ; do
    if [[ ! -v ${var} ]] ; then
        echo "Error: undefined ${var}" >&2
        exit 1
    fi
done

# Check libraries
if [[ -e "/usr/lib/phpboost-cli.bash/phpboost-cli.bash" ]] ; then
    . "/usr/lib/phpboost-cli.bash/phpboost-cli.bash"
else
    echo "Error: phpboost-cli missing." >&2
    exit 1
fi


echo 'Info: Get and update last run in a file'
last_run_file='/tmp/phpboost_antispam_bot_last_run'
if [[ -e "${last_run_file}" ]] ; then
    last_run=$(cat "${last_run_file}")
else
    echo "Warning: last run not recorded, act for the last two days" >&2
    last_run=$(date --date "2 days ago" +%s)
fi
echo "${current_run}" >"${last_run_file}"


SQL_EDIT="SELECT id, idtopic, user_id, display_name, content, from_unixtime(timestamp_edit) AS timestamp
FROM phpboost_forum_msg NATURAL JOIN phpboost_member
WHERE timestamp_edit >= ${last_run}
AND user_id = user_id_edit
AND posted_msg <= ${filter_user_max_msg}
ORDER BY timestamp_edit DESC"


function submit_wetterbericht() {
    # Get our server ip:
    var=( $(host "${webserver}") )
    server_ip=${var[-1]}
    phpboost_connect "${server_ip}" "${webserver}" "${LOGIN}" "${PASSWORD}"
    phpboost_add_msg "${reportTopic}" "${*}"
    phpboost_disconnect
}

function content_formater() {
    lined="$(echo "${*}" | sed -e 's|<\/TR>|<\/TR>\n|g')"
    header="$(echo "${lined}" | head -n 1 | sed -e 's|<TH>id<\/TH><TH>idtopic<\/TH><TH>user_id<\/TH><TH>display_name<\/TH>|<TH>name<br\/>link<\/TH>|')"
    inhalt="$(echo "${lined}" | tail -n +2 | sed -e 's|^<TR><TD>\([[:digit:]]\+\)<\/TD><TD>\([[:digit:]]\+\)<\/TD><TD>\([[:digit:]]\+\)<\/TD><TD>\([^<]\+\)<\/TD>|<TR><TD><a href="\/user\/profile\/\3">\4<\/a><br\/><a href="\/forum\/topic-\2.php#m\1">message<\/a><\/TD>|;s|<TD>\([-[:digit:]]\+\) \([:[:digit:]]\+\)<\/TD><\/TR>$|<TD>\1<br\/>\2<\/TD><\/TR>|')"
    echo "[html]${header}"
    echo "${inhalt}[/html]"
}

echo 'Info: Scanning the data-base.'
edit_results="$(echo "${SQL_EDIT}" | mysql -u "${bot_user}" -p${PASSWORD} "${data_base}" -H | tr -d '\r' | tr -d '\n' )"

if [[ ${#edit_results} -gt 0 ]] ; then
    echo "Info: Spam detected, sending report."
    submit_wetterbericht "$(content_formater "${edit_results}")" | grep '^HTTP*'
else
    echo "Info: empty results: no spam detected."
fi

echo "Info: script run within $(($(date +%s)-current_run)) seconds."

exit 0
